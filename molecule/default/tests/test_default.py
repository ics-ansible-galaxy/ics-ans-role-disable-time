import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_ntpd_is_running_and_enabled(host):
    ntpd = host.service("ntpd.service")
    assert not ntpd.is_enabled
    assert not ntpd.is_running


def test_chronyd_is_running_and_enabled(host):
    chronyd = host.service("chronyd.service")
    assert not chronyd.is_enabled
    assert not chronyd.is_running
