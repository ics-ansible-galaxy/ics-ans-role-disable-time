# ics-ans-role-disable-time

Ansible role to install disable system time for virtual machines.
The role will disable ntpd and chronyd.

## Requirements

- ansible >= 2.4
- molecule >= 2.6

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-disable-time
```

## License

BSD 2-clause
